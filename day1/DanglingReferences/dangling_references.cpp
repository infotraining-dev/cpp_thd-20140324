#include <iostream>
#include <boost/thread.hpp>
#include <boost/noncopyable.hpp>
#include <boost/thread/thread_guard.hpp>
#include <boost/thread/scoped_thread.hpp>

using namespace std;

class Worker
{
    string& text_;
public:
    Worker(string& text) : text_(text)
    {}

    void operator()()
    {
        for(size_t i = 0; i < text_.length(); ++i)
        {
            cout << "#" << text_[i] << "#" << endl;
        }
    }
};

void may_throw()
{
    throw std::runtime_error("Error");
}

// RAII
class ThreadGuard : boost::noncopyable
{
    boost::thread& thread_;
public:
    explicit ThreadGuard(boost::thread& thd) :thread_(thd) {}

    ~ThreadGuard()
    {
        if (thread_.joinable())
            thread_.join();
    }
};

void local1()
{
    string text = "Thread";

    Worker w(text);
    boost::thread thd(w);
    //ThreadGuard thread_guard(thd);
    boost::thread_guard<> tg(thd);

    may_throw();
}

void local2()
{
    string text = "Thread";

    Worker w(text);

    boost::thread thd(w);
    boost::scoped_thread<> st(boost::move(thd));

    may_throw();
}

int main()
{
    try
    {
        //local1();
        local2();
    }
    catch(const runtime_error& e)
    {
        cout << e.what() << endl;
    }

    boost::this_thread::sleep_for(boost::chrono::seconds(5));
}
