#include <iostream>
#include <vector>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

#include <thread>

using namespace std;

void background_work(int id, int count)
{
    for(int i = 0; i < count; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;
        boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
    }
}

class Task
{
    int id_;
public:
    Task(int id) : id_(id) {}

    void operator()(const string& text)
    {
        for(size_t i = 0; i < text.length(); ++i)
        {
            cout << "Task#" << id_ << ": " << text[i] << endl;
            boost::this_thread::sleep_for(boost::chrono::seconds(1));
        }
    }
};

class Buffer
{
    vector<int> buffer_;
public:
    void copy(const vector<int>& source)
    {
        buffer_.assign(source.begin(), source.end());
    }

    vector<int> data() const
    {
        return buffer_;
    }
};

void run()
{
    boost::thread thd(&background_work, 99, 100);

    thd.detach();

    assert(!thd.joinable());
}

void copy_buffers()
{
    vector<int> data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    Buffer buff1;
    Buffer buff2;

    //boost::thread thd_copy_1(boost::bind(&Buffer::copy, &buff1, boost::cref(data)));

    boost::thread thd_copy_1([&buff1, &data]() { buff1.copy(data); });

    buff2.copy(data);

    thd_copy_1.join();

    cout << "Buffer1: ";
    for(auto x : buff1.data())
    {
        cout << x << " ";
    }
    cout << "\n";

    cout << "Buffer2: ";
    for(auto x : buff2.data())
    {
        cout << x << " ";
    }
    cout << "\n";
}

int main()
{   
    copy_buffers();

    run();

    boost::thread thd1(&background_work, 1, 20);

    Task taskA(1);
    boost::thread thdA(taskA, "TEXT");
    boost::thread thdB(Task(2), "text");

    background_work(2, 10);

    boost::thread thd3(&background_work, 3, 5);

    thd1.join();
    thd3.join();
    thdA.join();
    thdB.join();
}
