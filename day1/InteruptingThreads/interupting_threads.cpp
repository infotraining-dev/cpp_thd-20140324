#include <iostream>
#include <boost/thread.hpp>

using namespace std;

void background_worker(int id, int count)
{
    cout << boost::this_thread::get_id() << endl;

    try
    {
        for(int i = 0; i < count; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;
            boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
            //boost::this_thread::interruption_point();
        }
    }
    catch(const boost::thread_interrupted& e)
    {
        cout << "Interupted thread!" << endl;
    }
}

int main()
{
    boost::thread thd1(&background_worker, 1, 100);

    boost::this_thread::sleep_for(boost::chrono::seconds(3));

    thd1.interrupt();

    cout << "Po interupt..." << endl;

    thd1.join();

    cout << "\n\nThreadGroup: " << endl;

    boost::thread_group tg;

    tg.add_thread(new boost::thread(&background_worker, 2, 10));
    tg.add_thread(new boost::thread(&background_worker, 3, 10));
    tg.create_thread(boost::bind(&background_worker, 4, 10));

    boost::thread* ptr_thd = new boost::thread(&background_worker, 5, 10);
    tg.add_thread(ptr_thd);

    // after while
    boost::this_thread::sleep_for(boost::chrono::seconds(2));
    cout << boolalpha << "Is thread in group: " << tg.is_thread_in(ptr_thd) << endl;

    tg.interrupt_all();

    tg.join_all();
}
