#include <iostream>
#include <boost/thread.hpp>

using namespace std;

void background_work(int id, int count)
{
    for(int i = 0; i < count; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;
        boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
    }
}

boost::thread create_thread()
{
    static int id = 1;

    return boost::thread(&background_work, id++, 10);
}

int main()
{
    cout << "Wątki sprzętowe: " << boost::thread::hardware_concurrency() << endl;

    boost::thread thd1 = create_thread();

    vector<boost::thread> vec_threads;
    vec_threads.push_back(create_thread());
    vec_threads.push_back(create_thread());
    vec_threads.push_back(std::move(thd1));

    for(auto& thd : vec_threads)
        thd.join();

    cout << "\n\n";

    boost::thread thd2 = create_thread();

    boost::thread thd_array[5];
    thd_array[0] = std::move(thd2);

    thd_array[0].join();
}
