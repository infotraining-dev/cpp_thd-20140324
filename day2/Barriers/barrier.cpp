#include <iostream>
#include <boost/thread.hpp>

using namespace std;

boost::barrier thread_barrier(3);

void task(int id, int timeout)
{
    cout << "Task#" << id << " started..." << endl;
    boost::this_thread::sleep_for(boost::chrono::seconds(timeout));

    thread_barrier.wait();

    cout << "Task#" << id << " resumed..." << endl;
    boost::this_thread::sleep_for(boost::chrono::seconds(timeout));
    thread_barrier.wait();
    cout << "Task#" << id << " finished..." << endl;
}

int main()
{
    boost::thread_group thd_group;

    thd_group.create_thread(boost::bind(&task, 1, 3));
    thd_group.create_thread(boost::bind(&task, 2, 5));
    thd_group.create_thread(boost::bind(&task, 3, 1));

    thd_group.join_all();
}
