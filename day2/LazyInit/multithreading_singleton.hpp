#ifndef MULTITHREADING_SINGLETON_HPP
#define MULTITHREADING_SINGLETON_HPP

#include <boost/thread.hpp>

namespace Multithreading
{
	template<typename T>
	class SingletonHolder : boost::noncopyable
	{
	private:
		SingletonHolder();
		static T* instance_;
		static boost::once_flag init_flag_;
		static void init_instance()
		{
			instance_ = new T();
		}
	public:
		static T& instance()
		{
			boost::call_once(init_flag_, SingletonHolder<T>::init_instance);
			return *instance_;
		}
	};

	template<typename T>
	T* SingletonHolder<T>::instance_;

	template<typename T>	
	boost::once_flag SingletonHolder<T>::init_flag_ = BOOST_ONCE_INIT;
}

#endif
