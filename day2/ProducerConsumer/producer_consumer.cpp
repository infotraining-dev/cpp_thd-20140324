#include <iostream>
#include <string>
#include <queue>
#include <algorithm>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "thread_safe_queue.hpp"

using namespace std;

namespace Before
{

class ProducerConsumer
{
    typedef string DataType;

    boost::mutex mtx_;
    boost::condition_variable cond_;

    queue<DataType> queue_;

public:
    void produce()
    {
        DataType data = "abcd";

        while (next_permutation(data.begin(), data.end()))
        {
            cout << "Producing: " << data << endl;
            boost::unique_lock<boost::mutex> lk(mtx_);
            queue_.push(data);
            cond_.notify_one(); // powiadomienie o wystapieniu warunku
            lk.unlock();
            boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
        }

        cout << "End of production." << endl;
        boost::lock_guard<boost::mutex> lk(mtx_);
        queue_.push("");
        queue_.push("");
        cond_.notify_all();
    }

    void consumer(int id)
    {
        while (true)
        {
            boost::unique_lock<boost::mutex> lk(mtx_);
            //cond_.wait(lk, [&]() { return !queue_.empty(); });
            cond_.wait(lk, !boost::bind(&queue<DataType>::empty, &queue_));

            DataType data = queue_.front();
            queue_.pop();

            lk.unlock();

            if (data == "")
            {
                cout << "End of consumer's thread#" << id << endl;
                return;
            }

            process_data(id, data);
        }
    }

private:
    void process_data(int id, const DataType& data)
    {
        cout << "Consumer#" << id << " is processing: " << data << endl;
        boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
    }
};

}

namespace After
{
    class ProducerConsumer
    {
        typedef string DataType;
        ThreadSafeQueue<DataType> queue_;

    public:
        void produce()
        {
            DataType data = "abcd";

            while (next_permutation(data.begin(), data.end()))
            {
                cout << "Producing: " << data << endl;
                queue_.push(data);
                boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
            }

            cout << "End of production." << endl;
            queue_.push("");
            queue_.push("");
        }

        void consumer(int id)
        {
            while (true)
            {
                DataType data;
                queue_.wait_and_pop(data);

                if (data == "")
                {
                    cout << "End of consumer's thread#" << id << endl;
                    return;
                }

                process_data(id, data);
            }
        }

    private:
        void process_data(int id, const DataType& data)
        {
            cout << "Consumer#" << id << " is processing: " << data << endl;
            boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
        }
    };
}

int main()
{
    using namespace After;

    ProducerConsumer pc;

    boost::thread thd_producer(boost::bind(&ProducerConsumer::produce, &pc));
    boost::thread thd_consumer1(boost::bind(&ProducerConsumer::consumer, &pc, 1));
    boost::thread thd_consumer2(boost::bind(&ProducerConsumer::consumer, &pc, 2));

    thd_producer.join();
    thd_consumer1.join();
    thd_consumer2.join();
}
