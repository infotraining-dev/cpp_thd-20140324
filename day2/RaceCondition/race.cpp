#include <iostream>
#include <atomic>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

using namespace std;

const int NO_OF_ITERATIONS = 10000000;

int counter = 0;
boost::mutex mtx;

class Counter
{
    int value_;
    mutable boost::mutex mtx_;
public:
    Counter(int value) : value_(value) {}

    int value() const
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        return value_;
    }

    Counter& operator++()
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        ++value_;

        return *this;
    }
};

void may_throw()
{
    //throw std::runtime_error("Error");
}

void do_stuff()
{
}

void worker()
{
    try
    {
        boost::unique_lock<boost::mutex> lk(mtx);
        for (int i = 0 ; i < NO_OF_ITERATIONS ; ++i)
        {
            counter++;
            may_throw();
            lk.unlock();
            do_stuff();
        }
    } catch(...)
    {

    }
}

atomic<int> atomic_counter;

void worker_with_atomic()
{
    try
    {
        for (int i = 0 ; i < NO_OF_ITERATIONS ; ++i)
        {
            atomic_counter++;
            may_throw();
            do_stuff();
        }
    } catch(...)
    {

    }
}

Counter counter_with_monitor(0);

void worker_with_monitor()
{
    for (int i = 0 ; i < NO_OF_ITERATIONS ; ++i)
    {
        ++counter_with_monitor;
        may_throw();
        do_stuff();
    }
}

int main()
{
    {
        auto start = boost::chrono::high_resolution_clock::now();
        boost::thread th1(worker);
        boost::thread th2(worker);
        th1.join();
        th2.join();
        auto end = boost::chrono::high_resolution_clock::now();
        float seconds = boost::chrono::duration_cast<boost::chrono::milliseconds>(end-start).count();
        std::cout << "Time: " << seconds << " msec" << std::endl;

        cout << "Expected = " << NO_OF_ITERATIONS * 2 << endl;
        cout << "Counter = " << counter << endl;
    }

    cout << "\n\n";

    {
        auto start = boost::chrono::high_resolution_clock::now();
        boost::thread th1(worker_with_atomic);
        boost::thread th2(worker_with_atomic);
        th1.join();
        th2.join();
        auto end = boost::chrono::high_resolution_clock::now();
        float seconds = boost::chrono::duration_cast<boost::chrono::milliseconds>(end-start).count();
        std::cout << "Time: " << seconds << " msec" << std::endl;

        cout << "Expected = " << NO_OF_ITERATIONS * 2 << endl;
        cout << "Counter = " << atomic_counter << endl;
    }

    cout << "\n\n";

    {
        auto start = boost::chrono::high_resolution_clock::now();
        boost::thread th1(worker_with_monitor);
        boost::thread th2(worker_with_monitor);
        th1.join();
        th2.join();
        auto end = boost::chrono::high_resolution_clock::now();
        float seconds = boost::chrono::duration_cast<boost::chrono::milliseconds>(end-start).count();
        std::cout << "Time: " << seconds << " msec" << std::endl;

        cout << "Expected = " << NO_OF_ITERATIONS * 2 << endl;
        cout << "Counter = " << counter_with_monitor.value() << endl;
    }
}
