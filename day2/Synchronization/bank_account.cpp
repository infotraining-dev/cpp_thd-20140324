#include <boost/thread.hpp>
#include <boost/thread/lockable_adapter.hpp>
#include <iostream>

using namespace std;

class BankAccount
{
    const int id_;
    double balance_;
    mutable boost::mutex mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print()
    {
        std::cout << "Bank Account " << id_ << std::endl;
        boost::lock_guard<boost::mutex> lk(mtx_);
        std::cout << "balance = " << balance_ << std::endl;
    }

    void transfer(BankAccount& target, double amount)
    {

        boost::unique_lock<boost::mutex> lk(mtx_, boost::defer_lock);
        boost::unique_lock<boost::mutex> lk_target(target.mtx_, boost::defer_lock);

        boost::lock(lk, lk_target);

        balance_ -= amount;
        target.balance_ += amount;
    }

    void withdraw(double amount)
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        boost::lock_guard<boost::mutex> lk(mtx_);
        return balance_;
    }
};

void test_transfer(BankAccount& ba1, BankAccount& ba2, int no_of_operations, int thd_id)
{
    for(int i = 0; i < no_of_operations; ++i)
    {
        cout << "THD#" << thd_id
             <<  "Transfer from ba#" << ba1.id() << " to ba#" << ba2.id() << endl;

        ba1.transfer(ba2, 1.0);
    }
}

int main()
{
    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    boost::thread thd1(&test_transfer, boost::ref(ba1), boost::ref(ba2), 10000, 1);
    boost::thread thd2(&test_transfer, boost::ref(ba2), boost::ref(ba1), 10000, 2);

    thd1.join();
    thd2.join();

    cout << "Po przelewach:" << endl;

    ba1.print();
    ba2.print();
}
