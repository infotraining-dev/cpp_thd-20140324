#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

using namespace std;

boost::timed_mutex mutex;

void background_worker(int id, int timeout)
{
    cout << "BW#" << id << " is waiting for mutex..." << endl;

    boost::unique_lock<boost::timed_mutex> lk(mutex, boost::try_to_lock);
    if (!lk.owns_lock())
    {
        do
        {
            cout << "Thread doesn't own a lock... Tries to acqire a mutex..." << endl;
            boost::this_thread::sleep_for(boost::chrono::seconds(timeout));
        } while (!lk.try_lock());
    }

    cout << "Start BW#" << id << endl;

    boost::this_thread::sleep_for(boost::chrono::seconds(10));
}

int main()
{
    boost::thread thd1(&background_worker, 1, 2);
    boost::thread thd2(&background_worker, 2, 1);

    thd1.join();
    thd2.join();
}
