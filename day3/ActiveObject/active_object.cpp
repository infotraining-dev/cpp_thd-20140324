#include <iostream>
#include <fstream>
#include <boost/thread.hpp>
#include <boost/function.hpp>
#include "thread_safe_queue.hpp"

using namespace std;

typedef boost::function<void()> Task;
#include "thread_safe_queue.hpp"

using namespace std;

typedef boost::function<void()> Task;

class ActiveObject
{
public:
    ActiveObject() : is_done_(false)
    {
        thread_ = boost::thread(&ActiveObject::run, this);
    }

    ~ActiveObject()
    {
        send([&] { is_done_ = true; });

        thread_.join();
    }

    void send(Task task)
    {
        task_queue_.push(task);
    }
private:
    ThreadSafeQueue<Task> task_queue_;
    bool is_done_;
    boost::thread thread_;

    void run()
    {
        while (true)
        {
            Task task;
            task_queue_.wait_and_pop(task);

            task();

            if (is_done_)
                return;
        }
    }
};

class Document
{
    string content_;
    ActiveObject active_object_;
public:
    void add_text(const string& text)
    {
        content_ += text;
    }

    void save(const string& filename)
    {
        active_object_.send([=]() { save_to_file(filename);});
    }
private:
    void save_to_file(const string& filename)
    {
        cout << "Start saving to file: " << filename << endl;
        ofstream fout(filename);

        fout << content_ << endl;

        fout.close();
        boost::this_thread::sleep_for(boost::chrono::seconds(3));
        cout << "Start saving to file: " << filename << endl;
    }
};

int main()
{
    Document doc;

    doc.add_text("Linia1\n");
    doc.add_text("Linia2\n");

    doc.save("doc.txt");

    for(int i = 0; i < 10; ++i)
    {
        cout << "Main thread is working..." << endl;
        boost::this_thread::sleep_for(boost::chrono::milliseconds(250));
    }
}
