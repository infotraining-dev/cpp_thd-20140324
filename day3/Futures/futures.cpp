#include <iostream>
#include <boost/thread.hpp>

using namespace std;

int calculate(int x)
{
    int interval = rand() % 5000;

    boost::this_thread::sleep_for(boost::chrono::milliseconds(interval));

    if (x == 13)
        throw std::runtime_error("Error#13");

    return x * x;
}

class SquareCalculator
{
    boost::promise<int> promise_;
public:
    void operator()(int x)
    {
        int interval = rand() % 5000;

        boost::this_thread::sleep_for(boost::chrono::milliseconds(interval));

        promise_.set_value(x * x);
    }

    boost::unique_future<int> get_future()
    {
        return promise_.get_future();
    }
};

boost::shared_future<int> calculate_async(int x)
{
    return boost::async(boost::bind(&calculate, x));
}

int main()
{
    int result = calculate(10);

    vector<boost::thread> vec_threads(8);

    boost::packaged_task<int> pt(boost::bind(&calculate, 10));
    boost::unique_future<int> fresult = pt.get_future();
    vec_threads[0] = boost::thread(boost::move(pt));

    //boost::unique_future<int> fresult = boost::async(boost::bind(&calculate, 10));

    while(!fresult.is_ready())
    {
        cout << "Main thread is working..." << endl;
        boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
    }

    cout << "result: " << fresult.get() << endl;

    vector<boost::unique_future<int>> vec_fresults;

    for(int i = 0; i < 15; ++i)
    {
        vec_fresults.push_back(boost::async(boost::bind(&calculate, i)));
    }

    boost::wait_for_all(vec_fresults.begin(), vec_fresults.end());

    for(auto& fr : vec_fresults)
    {
        try
        {
            cout << fr.get() << "; ";
        }
        catch(const exception& e)
        {
            cout << e.what() << endl;
        }
    }
    cout << endl;

    boost::shared_future<int> fresult_shared = calculate_async(25);

    boost::shared_future<int> copy_fresult = fresult_shared;

    cout << "\nresult async:" << copy_fresult.get() << endl;

    cout << "\nPromise:\n";

    SquareCalculator scalc;

    boost::unique_future<int> fsquare = scalc.get_future();

    boost::thread thd(boost::ref(scalc), 99);

    cout << "Result from scalc: " << fsquare.get() << endl;
}
