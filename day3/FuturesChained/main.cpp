#include <iostream>
#include <fstream>
#include <deque>
#include <thread>
#include <future>
#include <chrono>
#include <map>
#include <boost/tokenizer.hpp>

using namespace std;

typedef deque<string> FileContent;

FileContent load_file(const string& file_name)
{
    FileContent file_content;

    fstream fin(file_name);

    if (!fin)
        throw std::runtime_error("File error");

    while (fin)
    {
        string line;
        getline(fin, line);

        file_content.push_back(line);
    }

    fin.close();

    return std::move(file_content);
}

typedef std::map<string, int> ConcordanceType;

std::map<string, int> make_concordance(const FileContent& file_content)
{
    std::map<string, int> concordance;

    for (const auto& line : file_content)
    {
        boost::tokenizer<> tok(line);

        for (const auto& word : tok)
            concordance[word]++;
    }

    return std::move(concordance);
}

int main()
{
    std::shared_future<FileContent> fcontent = std::async(&load_file, "../main.cpp");

    auto fconcordance = std::async([=]() { return make_concordance(fcontent.get()); });

    // C++14
    //fcontent.then([](std::future<FileContent> f) { return make_concordance(f.get())});

    for (const auto& p : fconcordance.get())
        cout << p.first << " - " << p.second << endl;
}
