#include <iostream>
#include <thread>
#include <chrono>
#include <future>
#include <functional>
#include <vector>
#include <algorithm>
#include <boost/bind.hpp>

using namespace std;

namespace CPP11
{

    template <typename It, typename T>
    T parallel_accumulate(It first, It last, T init)
    {
        // parallel version of accumulate
        size_t len = std::distance(first, last);
        if (!len) return init;
        unsigned int hw_thd = std::thread::hardware_concurrency();
        hw_thd = 2;
        size_t block_size = len/hw_thd;

        std::vector<std::future<T>> results;

        It block_start = first;

        for (size_t i = 0 ; i < hw_thd-1 ; ++i)
        {
            It block_end = block_start;
            std::advance(block_end, block_size);
            results.push_back( std::async( std::launch::async,
                                           std::accumulate<It, T>,
                                           block_start, block_end, T() ));
            block_start = block_end;
        }

        init += std::accumulate(block_start, last, init);

    //    return std::accumulate(results.begin(), results.end(),
    //                           init, [] (T a, future<T> &b) -> T { return a + b.get(); });

        return std::accumulate(results.begin(), results.end(),
                               init,
                               boost::bind(std::plus<T>(), _1,
                                           boost::bind(&std::future<T>::get, _2)));
    }
}

int main()
{
    std::cout << "Hardware threads: "  << std::thread::hardware_concurrency() << std::endl;
    const size_t SIZE = 50000000;
    std::vector<long> v(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v[i] = i;
    }

    auto start = std::chrono::high_resolution_clock::now();
    std::cout << "Accumulate:          " << std::accumulate(v.begin(), v.end(), 0);
    auto end = std::chrono::high_resolution_clock::now();
    float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "   " << seconds << " msec" << std::endl;

    std::vector<long> v2(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v2[i] = i;
    }


    start = std::chrono::high_resolution_clock::now();
    std::cout << "Parallel Accumulate: " << CPP11::parallel_accumulate(v2.begin(), v2.end(), 0);
    end = std::chrono::high_resolution_clock::now();
    seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
    std::cout << "   " << seconds << " msec" << std::endl;
    return 0;
}
