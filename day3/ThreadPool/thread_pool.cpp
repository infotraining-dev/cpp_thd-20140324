#include <iostream>
#include <boost/thread.hpp>
#include <boost/function.hpp>
#include "thread_safe_queue.hpp"

using namespace std;

void do_something(const string& data)
{
    boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));

    cout << "Done: " << data << endl;
}

void foo()
{
    cout << "foo()" << endl;
}

class Foo
{
public:
    void operator()()
    {
        cout << "Foo::operator()()" << endl;
    }
};

typedef boost::function<void()> Task;

class ThreadPool : boost::noncopyable
{
    static const Task END_OF_WORK;

    size_t no_of_threads_;
    ThreadSafeQueue<Task> task_queue_;
    boost::thread_group threads_;

    void run()
    {
        while (true)
        {
            Task task;
            task_queue_.wait_and_pop(task);

            if (!task)
                return;

            task(); // wywolanie zadania w watku puli
        }
    }

public:
    ThreadPool(size_t no_of_threads) : no_of_threads_(no_of_threads)
    {
        for(size_t i = 0; i < no_of_threads_; ++i)
            threads_.create_thread(boost::bind(&ThreadPool::run, this));
    }

    ~ThreadPool()
    {
        for(size_t i = 0; i < no_of_threads_; ++i)
            task_queue_.push(END_OF_WORK);

        threads_.join_all();
    }

    void enqueue(Task task)
    {
        task_queue_.push(task);
    }
};

const Task ThreadPool::END_OF_WORK;

int main()
{
    ThreadPool thread_pool(8);

    boost::function<void()> f = &foo;
    f();

    Foo foonctor;
    f = foonctor;
    f();

    f = boost::bind(&do_something, "Text");
    f();

    thread_pool.enqueue(f);

    thread_pool.enqueue(f);
    thread_pool.enqueue(&foo);
    thread_pool.enqueue(Foo());
}
