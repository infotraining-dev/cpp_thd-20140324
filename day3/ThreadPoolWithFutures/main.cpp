#include <iostream>
#include <deque>
#include <fstream>

#include "thread_pool.hpp"

using namespace std;

std::mutex cout_mtx;

typedef std::deque<string> FileContent;

FileContent load_file(const string& file_name)
{
    FileContent file_content;

    fstream fin(file_name);

    if (!fin)
        throw std::runtime_error("File error");

    while (fin)
    {
        string line;
        getline(fin, line);

        file_content.push_back(line);
    }

    fin.close();

    return std::move(file_content);
}

int main(int argc, char *argv[])
{
    After::ThreadPool pool(8);

    std::vector< boost::unique_future<int> > results;

    for(int i = 0; i < 8; ++i) {
        results.push_back(
                    pool.enqueue<int>([i] {

                        {
                            std::lock_guard<std::mutex> lock(cout_mtx);
                            std::cout << "start " << i << std::endl;
                        }

                        std::this_thread::sleep_for(std::chrono::seconds(2));

                        {
                            std::lock_guard<std::mutex> lock(cout_mtx);
                            std::cout << "finish " << i << std::endl;
                        }
                        return i*i;
                })
          );
    }

    for(size_t i = 0;i<results.size();++i)
    {
        std::cout << results[i].get() << ' ';
    }

    std::cout << "\n\n" << std::endl;

    std::vector<boost::unique_future<FileContent> > files;

    std::vector<string> file_names = { "../main.cpp", "../thread_pool.hpp", "../thread_safe_queue.hpp" };

    for(auto& fn : file_names)
        files.push_back(pool.enqueue<FileContent>([=]() { return load_file(fn);}));

    for(auto& f : files)
        cout << f.get().size() << endl;

    return 0;
}
