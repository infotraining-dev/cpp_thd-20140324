#pragma once

#include <iostream>
#include <thread>
#include <mutex>
#include <queue>
#include <vector>
#include <functional>
#include <future>
#include <stdexcept>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/function.hpp>

#include "thread_safe_queue.hpp"

namespace After
{
    class ThreadPool : boost::noncopyable
    {
        typedef boost::function<void ()> Task;

        std::vector<boost::thread> workers_;
        ThreadSafeQueue<Task> tasks_;

    public:
        ThreadPool();
        ThreadPool(int no_of_threads);
        ~ThreadPool();

        template<typename T, typename F>
        boost::unique_future<T> enqueue(F fun);

    private:
        void do_background_work();
        void init_workers(int no_of_workers);
        void end_of_background_work();
    };

    ThreadPool::ThreadPool()
    {
        int no_of_threads = std::thread::hardware_concurrency();

        if (no_of_threads == 0) no_of_threads = 2;

        init_workers(no_of_threads);
    }

    ThreadPool::ThreadPool(int no_of_threads)
    {
        init_workers(no_of_threads);
    }

    void ThreadPool::init_workers(int no_of_threads )
    {
        for (size_t i = 0 ; i < no_of_threads ; ++i)
        {
            workers_.emplace_back(boost::thread(&ThreadPool::do_background_work, this));
        }
    }

    void ThreadPool::end_of_background_work()
    {
        Task end_of_work;
        for(size_t i = 0; i < workers_.size(); ++i)
            tasks_.push(end_of_work);
    }

    ThreadPool::~ThreadPool()
    {
        end_of_background_work();

        for (auto &worker : workers_)
        {
            worker.join();
        }
    }

    template<typename T, typename F>
    boost::unique_future<T> ThreadPool::enqueue(F fun)
    {
        auto task = boost::make_shared<boost::packaged_task<T> >(fun);
        boost::unique_future<T> fresult = task->get_future();
        tasks_.push([task]() { (*task)(); });

        return fresult;
    }

    void ThreadPool::do_background_work()
    {
        while (true)
        {
            Task task;
            tasks_.wait_and_pop(task);

            if (task)
                task();
            else
                return;
        }
    }
}
